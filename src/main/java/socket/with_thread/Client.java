package socket.with_thread;

import shemshadi.Utility.Utilities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        try {
            InetAddress ip = InetAddress.getByName("localhost");
            Socket socket = new Socket(ip, 5056);

            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

            while (true){
                System.out.println(dataInputStream.readUTF());
                String toSend = Utilities.getInstance().getStringFromUser();
                dataOutputStream.writeUTF(toSend);

                if(toSend.equals("exit")){
                    System.out.println("closing connection ... ");
                    socket.close();
                    System.out.println("connection closed.");
                    break;
                }

                String received = dataInputStream.readUTF();
                System.out.println(received);
            }
            socket.close();
            dataInputStream.close();
            dataOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
