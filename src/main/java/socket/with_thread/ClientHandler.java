package socket.with_thread;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientHandler extends Thread {
    DateFormat forDate = new SimpleDateFormat("yyy/mm/dd");
    DateFormat forTime = new SimpleDateFormat("hh:mm:ss");
    DataInputStream dataInputStream = null;
    DataOutputStream dataOutputStream = null;
    Socket socket = null;

    ClientHandler(DataInputStream input, DataOutputStream output, Socket socket) {
        dataInputStream = input;
        dataOutputStream = output;
        this.socket = socket;
        System.out.println("thread created");
    }

    @Override
    public void run() {
        System.out.println("thread started");
        String resived = "";
        String toReturn = "";
        while (true) {
            try {
                dataOutputStream.writeUTF("What do you want?[Date | Time]..\n" +
                        "Type Exit to terminate connection.");
                resived = dataInputStream.readUTF();

                if (resived.equals("exit")) {
                    this.socket.close();
                    System.out.println("close socket");
                    System.out.println("close connection");
                    break;
                }

                Date date = new Date();

                switch (resived) {
                    case "date":
                        toReturn = forDate.format(date);
                        dataOutputStream.writeUTF(toReturn);
                        break;
                    case "time":
                        toReturn = forTime.format(date);
                        dataOutputStream.writeUTF(toReturn);
                        break;
                    default:
                        dataOutputStream.writeUTF("invalid input");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            dataOutputStream.close();
            dataInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
