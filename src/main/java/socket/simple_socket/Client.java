package socket.simple_socket;

import java.io.*;
import java.net.*;

public class Client {
    private Socket socket = null;
    private DataInputStream dataInputStream = null;
    private DataOutputStream dataOutputStream = null;

    public Client(String address, int port){
        try {
            socket = new Socket(address, port);
            System.out.println("connect");

            dataInputStream = new DataInputStream(System.in);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String res = "";

        while (!res.equals("over")){
            try {
                res = dataInputStream.readLine();
                dataOutputStream.writeUTF(res);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Client client = new Client("127.0.0.1", 5000);
    }
}
