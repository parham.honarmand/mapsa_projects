package socket.simple_socket;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;

public class Server {
    private Socket socket = null;
    private ServerSocket serverSocket = null;
    private DataInputStream inputStream = null;

    public Server(int port){
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("server started");

            socket = serverSocket.accept();
            System.out.println("client accepted");

            inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

            String res = "";

            while (!res.equals("over")){
                res = inputStream.readUTF();
                System.out.println(res);
            }

            inputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server(5000);
    }
}
