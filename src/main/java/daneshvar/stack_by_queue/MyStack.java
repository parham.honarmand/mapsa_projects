package daneshvar.stack_by_queue;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.stream.IntStream;

public class MyStack<T> {

    Queue<T> firstQueue = new ArrayDeque<>();

    public void push(T t){
        firstQueue.add(t);
    }

    public T pop(){
        IntStream.range(0, firstQueue.size() - 1).forEach(x -> firstQueue.add(firstQueue.poll()));
        return firstQueue.poll();
    }

    public int length(){
        return firstQueue.size();
    }

    public boolean empty() {
        return firstQueue.size() == 0;
    }
}
