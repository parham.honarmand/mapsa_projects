package daneshvar.toy_factory;

public enum ToySize {
    SMALL,
    MEDIUM,
    LARGE,
}
