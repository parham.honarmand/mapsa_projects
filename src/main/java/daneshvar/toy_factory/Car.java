package daneshvar.toy_factory;

public class Car extends Toy {

    public void Car(double basePrice, ToySize size){
        this.basePrice = basePrice;
        this.size = size;
        smallMultiple = 2;
        mediumMultiple = 2.5f;
        largeMultiple = 3;
    }
}
