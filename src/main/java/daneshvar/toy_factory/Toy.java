package daneshvar.toy_factory;

public class Toy {

    protected double basePrice;
    protected ToySize size;
    protected float smallMultiple;
    protected float mediumMultiple;
    protected float largeMultiple;

    public void toy(double basePrice, ToySize size){
        this.basePrice = basePrice;
        this.size = size;
        smallMultiple = 1f;
        mediumMultiple = 1.5f;
        largeMultiple = 2f;
    }

    public double getPrice(){
        return size == ToySize.SMALL ? basePrice*smallMultiple : size == ToySize.MEDIUM ?
                basePrice*mediumMultiple : basePrice*largeMultiple;
    }

    public double getPrice(double discount){
        return (getPrice() - (getPrice()*discount));
    }

    public double getBasePrice() {
        return basePrice;
    }
}
