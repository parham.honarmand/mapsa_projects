package daneshvar.lambda_expressions;

@FunctionalInterface
public interface BinaryOperator {

    Integer apply(Integer firstOperand, Integer secondOperator);
}
