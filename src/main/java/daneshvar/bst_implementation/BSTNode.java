package daneshvar.bst_implementation;

public class BSTNode<T extends Comparable> {
    T t;
    int depth;
    BSTNode<T> left, right;

    public BSTNode(T t, int depth) {
        this.t = t;
        this.depth = depth;
    }

    public void add(T t){
        if(this.t == null)
            this.t = t;
        else if (t.compareTo(this.t) < 0){
            if(left != null)
                left.add(t);
            else
                left = new BSTNode<T>(t, depth+1);
        }else if (t.compareTo(this.t) > 0){
            if(right != null)
                right.add(t);
            else
                right = new BSTNode<T>(t, depth+1);
        }
    }

    public int search(T t) {
        if(t.compareTo(this.t) == 0)
            return depth;
        if (t.compareTo(this.t) < 0) {
            if(left != null)
                return left.search(t);
            else
                return -1;
        }
        else {
            if(right != null)
                return right.search(t);
            else
                return -1;
        }
    }

    public T getT() {
        return t;
    }

    public boolean remove(T t, BSTNode<T> previous, NodeType type) {
        if(t.compareTo(this.t) == 0) {
            removeThis(previous, type);
            return true;
        }
        if (t.compareTo(this.t) < 0) {
            if(left != null) {
                return left.remove(t, this, NodeType.LEFT);
            }
            else
                return false;
        }
        else {
            if(right != null){
                return right.remove(t, this, NodeType.RIGHT);
            }else
                return false;
        }
    }

    private void removeThis(BSTNode<T> previous, NodeType type){
        if(type == null || previous == null)
            return;
        BSTNode<T> newNode = getAlternative();
        newNode.right = right;
        newNode.left = left;

        switch (type){
            case LEFT:
                previous.left = newNode;
                break;
            case RIGHT:
                previous.right = newNode;
                break;
        }
    }

    private BSTNode<T> getAlternative(){
        BSTNode<T> result;
        BSTNode<T> previous = null;

        if(left != null){
            result = left;
            while (result.right != null){
                previous = result;
                result = result.right;
            }
            if (previous != null) {
                if (previous.right.left != null)
                    previous.right = previous.right.left;
                else
                    previous.right = null;
            }
            return result;
        }else if (right != null){
            result = right;
            while (result.left == null){
                previous = result;
                result = result.left;
            }
            if(previous != null) {
                if(previous.left.right != null)
                    previous.left = previous.left.right;
                else
                    previous.left = null;
            }
            return result;
        }
        else
            return null;
    }

    public BSTNode<T> findMin() {
        BSTNode<T> result = this;
        while (result.left != null)
            result = result.left;
        return result;
    }

    public BSTNode<T> findMax() {
        BSTNode<T> result = this;
        while (result.right != null)
            result = result.right;
        return result;
    }
}
