package daneshvar.bst_implementation;

public class BSTree<T extends Comparable> {

    BSTNode<T> root;

    public void add(T t){
        if(root == null)
            root = new BSTNode<>(t, 0);
        else
            root.add(t);
    }

    public int search(T t){
        return (root == null)?-1:root.search(t);
    }

    public boolean remove(T t){
        return (root == null)?false:root.remove(t, null, null);
    }

    public BSTNode<T> findMin(){
        return (root == null)?null:root.findMin();
    }

    public BSTNode<T> findMax(){
        return (root == null)?null:root.findMax();
    }
}
