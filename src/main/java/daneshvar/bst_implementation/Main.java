package daneshvar.bst_implementation;

public class Main {

    public static void main(String[] args) {

        BSTree<Integer> tree = new BSTree<>();

        tree.add(100);
        tree.add(50);
        tree.add(125);
        tree.add(40);
        tree.add(60);
        tree.add(110);
        tree.add(140);
        tree.add(105);
        tree.add(112);
        tree.add(130);
        tree.add(170);
        tree.add(129);
        tree.add(132);

        System.out.println(tree.search(25));
        System.out.println(tree.search(44));

        System.out.println(tree.remove(140));

        System.out.println(tree.search(140));
        System.out.println(tree.root.right.right.t);

        System.out.println(tree.findMin().t);
        System.out.println(tree.findMax().t);
    }
}
