package daneshvar.linked_list;

public class Main {

    public static void main(String[] args) {
        MyLinkedList<String> linkedList = new MyLinkedList<>();

        linkedList.add("a");
        linkedList.add("b");
        linkedList.add("c");
        linkedList.add("d");
        linkedList.add("e");

        linkedList.remove(3);

        System.out.println(linkedList.find(3));
        System.out.println(linkedList.find(2));
        System.out.println(linkedList.find(4));
    }
}
