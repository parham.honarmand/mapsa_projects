package daneshvar.linked_list;

import java.util.stream.IntStream;

public class MyLinkedList<T> {

    int size = 0;
    MyNode<T> head;
    MyNode<T> last;

    public void add(T t){
        if (size == 0)
            head = new MyNode<>(null, t, null);
        MyNode newNode = new MyNode<>(last, t, null);
        if(size != 0)
        last.next = newNode;
        last = newNode;
        if(size == 1)
            head.next = last;

        size++;
    }

    public T find(int index){
        return search(index).t;
    }

    public void remove(int index){
        MyNode<T> node = search(index);
        node.previous.next = node.next;
        node.next.previous = node.previous;
    }

    private MyNode<T> search(int index){
        MyNode<T> current = head;
        for (int x = 0; x < index; x++) {
            current = current.next;
        }
        return current;
    }
}
