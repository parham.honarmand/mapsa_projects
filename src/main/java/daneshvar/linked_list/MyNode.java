package daneshvar.linked_list;

public class MyNode<T> {

    T t;
    MyNode<T> next;
    MyNode<T> previous;

    MyNode(MyNode<T> previous, T t, MyNode<T> next){
        this.t = t;
        this.next = next;
        this.previous = previous;
    }
}
