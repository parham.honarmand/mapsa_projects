package daneshvar.cost_management;

import java.util.*;

public class Accounting {
	List<Health> healthList = new ArrayList<>();
	Queue<Grocery> groceries = new LinkedList<>();
	Stack<HomeAppliance>homeAppliances = new Stack<>();
	
	public void addCost(Cost cost) {
		if(cost instanceof Health) {
			int len = healthList.size();
			for (int i = 0; i < healthList.size(); i++)
				if(healthList.get(i).getPriority() < ((Health) cost).getPriority()) {
					healthList.add(i, (Health) cost);
					break;
				}

			if(len == healthList.size())
				healthList.add((Health) cost);
		}
		else if (cost instanceof Grocery)
			groceries.add((Grocery) cost);
		else if (cost instanceof HomeAppliance)
			homeAppliances.add((HomeAppliance) cost);
	}

	public Cost getCost() {
		Cost result = null;
		if(healthList.size() != 0){
			result = healthList.get(0);
			healthList.remove(0);
		}
		else if (groceries.size() != 0)
			result = groceries.poll();
		else if(homeAppliances.size() != 0)
			result = homeAppliances.pop();
		return result;
	}

	public static void main(String[] args) {
		Accounting acc = new Accounting();
		acc.addCost(new Grocery(50000, "meat"));
		acc.addCost(new Health(100000, 1, "CalMagZink capsule"));
		acc.addCost(new HomeAppliance(4000000, "TV"));
		acc.addCost(new Health(60000, 4, "yearly check up"));
		acc.addCost(new Grocery(4000, "pancake powder"));
		acc.addCost(new HomeAppliance(600000, "pot"));
		
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
		System.out.println(acc.getCost());
	}

}
