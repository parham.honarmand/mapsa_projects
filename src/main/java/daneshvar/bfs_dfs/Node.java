package daneshvar.bfs_dfs;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
    private List<Node> neighbors = new ArrayList<>();
    private T t;
    private boolean isVisited = false;
    private int row;
    private int col;

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public List<Node> getNeighbors() {
        return neighbors;
    }

    public void addToNeighbors(Node node){
        neighbors.add(node);
    }
}
