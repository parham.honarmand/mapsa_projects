package daneshvar.bfs_dfs;

import daneshvar.queue_by_stack.MyQueue;
import org.omg.CosNaming.NamingContextPackage.NotFound;

public class BFSImpl<T> {
    MyQueue<Node> myQueue = new MyQueue<>();

    public Node<T> find(Node<T>[][] nodes, T t) throws NotFound {
        myQueue.inqueue(nodes[0][0]);
        nodes[0][0].setVisited(true);

        while (!myQueue.empty()) {
            Node<T> node = myQueue.dequeue();
            for (Node x : node.getNeighbors()) {
                if (t != x.getT() && !x.isVisited()) {
                    myQueue.inqueue(x);
                    x.setVisited(true);
                }
                else if (t == x.getT())
                    return x;
            }
        }
        throw new NotFound();
    }
}
