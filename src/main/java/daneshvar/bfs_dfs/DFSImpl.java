package daneshvar.bfs_dfs;

import daneshvar.stack_by_queue.MyStack;
import org.omg.CosNaming.NamingContextPackage.NotFound;

public class DFSImpl<T> {
    MyStack<Node<T>> myStack = new MyStack<>();

    public Node<T> find(Node<T>[][] nodes, Node<T> startedNode, T t) throws NotFound {
        myStack.push(startedNode);
        startedNode.setVisited(true);

        while (!myStack.empty()) {
            Node<T> node = myStack.pop();
            for (Node x : node.getNeighbors()) {
                if (t != x.getT() && !x.isVisited()) {
                    myStack.push(x);
                    System.out.println("row:"+x.getRow()+"col:"+x.getCol());
                    x.setVisited(true);
                }
                else if (t == x.getT()) {
                    return x;
                }
            }
        }
        throw new NotFound();
    }
}
