package daneshvar.bfs_dfs;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import shemshadi.Utility.Utilities;

public class Main {
    static Node[][] nodes;

    public static void main(String[] args) {
        System.out.println("enter row :");
        int row = Utilities.getInstance().getIntFromUser();
        System.out.println("enter col :");
        int col = Utilities.getInstance().getIntFromUser();
        setNodes(row, col);

        BFSImpl<String> bsf = new BFSImpl<>();
        Node startedNode = null;
        try {
            startedNode = bsf.find(nodes, "s");
            eraseVisited();
            System.out.println(startedNode.getRow());
            System.out.println(startedNode.getCol());
        } catch (NotFound notFound) {
            System.out.printf("bfs not found");
        }

        DFSImpl<String> dfs = new DFSImpl<>();
        if(startedNode != null) {
            try {
                Node node = dfs.find(nodes, startedNode, "t");
                System.out.println("\n" + node.getRow());
                System.out.println(node.getCol());
            } catch (NotFound e) {
                System.out.println(" dfs not found");
            }
        }
    }

    public static void setNodes(int row, int col){
        nodes = new Node[row][col];

        for (int i = 0; i < row; i++)
            for (int j = 0; j < col; j++)
                nodes[i][j] = new Node();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                nodes[i][j].setRow(i);
                nodes[i][j].setCol(j);
                if(i == 1 && j == 5)
                    nodes[i][j].setT("t");

                if(i == 4 && j == 5)
                    nodes[i][j].setT("t");

                if(i == 3 && j == 4)
                    nodes[i][j].setT("s");

                if(i>0)
                    if (nodes[i - 1][j] != null)
                        nodes[i][j].addToNeighbors(nodes[i - 1][j]);


                if(j>0)
                    if (nodes[i][j - 1] != null)
                        nodes[i][j].addToNeighbors(nodes[i][j - 1]);


                if(i!=row-1)
                    if(nodes[i+1][j] != null)
                        nodes[i][j].addToNeighbors(nodes[i+1][j]);

                if(j!=col-1)
                    if(nodes[i][j+1] != null)
                        nodes[i][j].addToNeighbors(nodes[i][j+1]);
            }
        }
    }

    private static void eraseVisited(){
        Node<String> node = nodes[0][0];

        for (int i = 0; i < nodes.length; i++)
            for (int j = 0; j < nodes[0].length; j++)
                nodes[i][j].setVisited(false);
    }
}
