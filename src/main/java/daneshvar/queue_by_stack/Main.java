package daneshvar.queue_by_stack;

public class Main {

    public static void main(String[] args) {
        MyQueue<Integer> queue = new MyQueue();

        queue.inqueue(20);
        queue.inqueue(30);
        queue.inqueue(40);
        queue.inqueue(50);
        queue.inqueue(2);
        queue.inqueue(3);
        queue.inqueue(44);

        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
    }
}
