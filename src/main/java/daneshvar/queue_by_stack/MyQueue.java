package daneshvar.queue_by_stack;

import java.util.Stack;
import java.util.stream.IntStream;

public class MyQueue<T> {

    Stack<T> firstStack = new Stack<>();
    Stack<T> secondStack = new Stack<>();

    public void inqueue(T t){
        firstStack.push(t);
    }

    public T dequeue(){
        if(firstStack.size() == 0)
            return null;
        IntStream.range(0, firstStack.size()).forEach(x -> secondStack.push(firstStack.pop()));
        T result = secondStack.pop();
        IntStream.range(0, secondStack.size()).forEach(x -> firstStack.push(secondStack.pop()));
        return result;
    }

    public int length(){
        return firstStack.size();
    }

    public boolean empty() {
        return firstStack.isEmpty();
    }
}
