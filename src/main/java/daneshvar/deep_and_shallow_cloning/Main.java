package daneshvar.deep_and_shallow_cloning;

/**
 * @author yourname
 */
public class Main {

    public static void main(String[] args) {

        Teacher teacher = new Teacher("ali", new Course(), new Book());

        Teacher teacher1 = null;
        try {
            teacher1 = teacher.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getCause());
        }
        System.out.println(teacher1);

        teacher.setName("joe");

        System.out.println(teacher1);
    }
}
