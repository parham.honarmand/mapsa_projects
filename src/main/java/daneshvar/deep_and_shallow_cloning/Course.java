package daneshvar.deep_and_shallow_cloning;

public class Course implements Cloneable {
    private String subject;

    //this is method returns a shallow copy
    @Override
    public Course clone() throws CloneNotSupportedException {
        return (Course) super.clone();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
