package daneshvar.tic_tac_toe;

import shemshadi.Utility.Utilities;

public class ViewManger {

    private static Board board;
    private static PlayerManager player;
    private static int row = -1;
    private static int col = -1;
    private static boolean isContinue = true;
    private static int input = -1;

    public static void main(String[] args) {

        System.out.println("Welcome to tic tac toe game");
        System.out.println("Lets play");

        firstMenu();
    }

    private static void firstMenu() {
        isContinue = true;
        board = new Board();
        System.out.println("1 - Two player");
        System.out.println("2 - One Player");
        System.out.println("enter a number :");

        int input = Utilities.getInstance().getIntFromUser();

        switch (input) {
            case 1:
                twoPlayerGamePlay();
                firstMenu();
                break;
            case 2:
                onePlayerGamePlay();
                firstMenu();
                break;
            default:
                firstMenu();
        }
    }

    private static void onePlayerGamePlay() {
        player = new PlayerManager(PlayerType.PLAYER, PlayerType.COMPUTER);
        while (isContinue) {

            do {
                board.showBoard();
                System.out.println(String.format("Its %s's turn", player.whoseTurn().type));

                if (player.whoseTurn().playerType == PlayerType.PLAYER) {
                    System.out.println("choose a cell");
                    try {
                        input = Utilities.getInstance().getIntFromUser();
                    } catch (Exception e) {
                        System.out.println("invalid input");
                    }
                } else {
                    input = board.computerChoice();
                }

            } while (!initCell(player.whoseTurn().type, input));
            thirdStep();
        }
    }

    public static void twoPlayerGamePlay() {
        player = new PlayerManager(PlayerType.PLAYER, PlayerType.PLAYER);

        while (isContinue) {
            do {
                board.showBoard();
                System.out.println(String.format("Its %s's turn", player.whoseTurn().type));
                System.out.println("choose a cell");

                try {
                    input = Utilities.getInstance().getIntFromUser();
                } catch (Exception e) {
                    System.out.println("invalid input");
                }

            } while (!initCell(player.whoseTurn().type, input));

            thirdStep();
        }
    }

    private static boolean initCell(CellType type, int input) {
        try {
            int[] coordinate = board.getRowAndCol(input);
            row = coordinate[0];
            col = coordinate[1];
        } catch (Exception notFound) {
            return false;
        }

        if (board.setACell(type, row, col))
            return true;

        System.out.println("this cell is already filled");
        return false;
    }

    private static void thirdStep() {
        if (board.checkForWinner(player.whoseTurn().type, row, col)) {
            board.showBoard();
            System.out.println(String.format("%s wins", player.whoseTurn().type));
            isContinue = false;
        } else if (board.checkForEnd()) {
            System.out.println("this game is even");
            isContinue = false;
        } else
            player.changeTurn();
    }
}
