package daneshvar.tic_tac_toe;

import shemshadi.Utility.Utilities;

public class PlayerManager {
    private Player turn;

    private Player player1;
    private Player player2;

    public PlayerManager(PlayerType player1, PlayerType player2){
        this.player1 = new Player(player1, CellType.X);
        this.player2 = new Player(player2, CellType.O);

        int random = Utilities.getInstance().getRandom(2);

        if(random == 0)
            turn = this.player1;
        else
            turn = this.player2;
    }

    public Player whoseTurn(){
        return turn;
    }

    public void changeTurn(){
        if ((turn == player1))
            turn = player2;
        else
            turn = player1;
    }
}
