package daneshvar.tic_tac_toe;

import shemshadi.Utility.Utilities;

public class Board {

    private final int ROWS = 3;
    private final int COLS = 3;
    private Cell[][] board;

    public Board() {
        board = new Cell[ROWS][COLS];
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                board[row][col] = new Cell();
            }
        }
    }

    public boolean setACell(CellType type, int row, int col) {
        if (board[row][col].isEmpty()) {
            board[row][col].init(type);
            return true;
        } else
            return false;
    }

    public void showBoard() {
        String result = String.format("" +
                        "------------------------" + "\n" +
                        "|   %s   |   %s   |   %s  |" + "\n" +
                        "|----------------------|" + "\n" +
                        "|   %s   |   %s   |   %s  |" + "\n" +
                        "|----------------------|" + "\n" +
                        "|   %s   |   %s   |   %s  |" + "\n" +
                        "|----------------------|" + "\n",
                (board[0][0].isEmpty()) ? "1" : board[0][0].getType(),
                (board[0][1].isEmpty()) ? "2" : board[0][1].getType(),
                (board[0][2].isEmpty()) ? "3" : board[0][2].getType(),
                (board[1][0].isEmpty()) ? "4" : board[1][0].getType(),
                (board[1][1].isEmpty()) ? "5" : board[1][1].getType(),
                (board[1][2].isEmpty()) ? "6" : board[1][2].getType(),
                (board[2][0].isEmpty()) ? "7" : board[2][0].getType(),
                (board[2][1].isEmpty()) ? "8" : board[2][1].getType(),
                (board[2][2].isEmpty()) ? "9" : board[2][2].getType());

        System.out.println(result);
    }

    public boolean checkForEnd() {
        for (Cell[] cells : board)
            for (Cell cell : cells)
                if (cell.isEmpty())
                    return false;
        return true;
    }

    public boolean checkForWinner(CellType type, int row, int col) {

        return (board[row][0].getType() == type && board[row][1].getType() == type
                && board[row][2].getType() == type)
                || (board[0][col].getType() == type && board[1][col].getType() == type
                && board[2][col].getType() == type)
                || (row == col && board[0][0].getType() == type && board[1][1].getType() == type
                && board[2][2].getType() == type)
                || (row + col == 2 && board[0][2].getType() == type && board[1][1].getType() == type
                && board[2][0].getType() == type);
    }

    public int computerChoice() {

        int random = Utilities.getInstance().getRandom(9);
        try {
            if (checkForEmpty(random))
                return random;
            else
                computerChoice();
        } catch (Exception notFound) {
            computerChoice();
        }
        return -1;
    }

    private boolean checkForEmpty(int num) throws Exception {
        int[] coordinate = getRowAndCol(num);
        if (board[coordinate[0]][coordinate[1]].isEmpty())
            return true;
        return false;
    }

    public int[] getRowAndCol(int input) throws Exception {
        int[] coordinates = new int[2];
        if (input == 1) {
            coordinates[0] = 0;
            coordinates[1] = 0;
        } else if (input == 2) {
            coordinates[0] = 0;
            coordinates[1] = 1;
        } else if (input == 3) {
            coordinates[0] = 0;
            coordinates[1] = 2;
        } else if (input == 4) {
            coordinates[0] = 1;
            coordinates[1] = 0;
        } else if (input == 5) {
            coordinates[0] = 1;
            coordinates[1] = 1;
        } else if (input == 6) {
            coordinates[0] = 1;
            coordinates[1] = 2;
        } else if (input == 7) {
            coordinates[0] = 2;
            coordinates[1] = 0;
        } else if (input == 8) {
            coordinates[0] = 2;
            coordinates[1] = 1;
        } else if (input == 9) {
            coordinates[0] = 2;
            coordinates[1] = 2;
        } else {
            throw new Exception();
        }
        return coordinates;
    }
}
