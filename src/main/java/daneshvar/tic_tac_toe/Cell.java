package daneshvar.tic_tac_toe;

public class Cell {

    private boolean isEmpty = true;
    private CellType cellType;

    public void init(CellType type){
        cellType = type;
        isEmpty = false;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public CellType getType(){
        return cellType;
    }
}
