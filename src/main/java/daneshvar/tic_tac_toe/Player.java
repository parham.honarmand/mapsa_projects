package daneshvar.tic_tac_toe;

public class Player {
    PlayerType playerType;
    CellType type;

    public Player(PlayerType playerType, CellType type) {
        this.playerType = playerType;
        this.type = type;
    }
}
