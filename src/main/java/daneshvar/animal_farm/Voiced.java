package daneshvar.animal_farm;

public interface Voiced {
    void makeSound();
}
