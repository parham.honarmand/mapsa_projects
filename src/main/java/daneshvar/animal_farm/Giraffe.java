package daneshvar.animal_farm;

public class Giraffe extends Animal {


    public Giraffe(Integer age) {
        super(age);
    }

    void eatFood() {
        System.out.println("giraffe eat food");
    }

    public void moveUp(Double amount) {
        setyPosition(getyPosition() + amount);
        System.out.println("giraffe moved up");
    }

    public void moveDown(Double amount) {
        setyPosition(getyPosition() - amount);
        System.out.println("giraffe moved down");
    }

    public void moveRight(Double amount) {
        setxPosition(getxPosition() + amount);
        System.out.println("giraffe moved right");
    }

    public void moveLeft(Double amount) {
        setxPosition(getxPosition() - amount);
        System.out.println("giraffe moved left");
    }
}
