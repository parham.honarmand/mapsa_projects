package daneshvar.animal_farm;

public interface Movable {
    void moveUp(Double amount);

    void moveDown(Double amount);

    void moveRight(Double amount);

    void moveLeft(Double amount);
}
