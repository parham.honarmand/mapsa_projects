package daneshvar.animal_farm;

public class Kiwi extends Bird implements Voiced {


    public Kiwi(Integer age) {
        super(age);
    }

    void eatFood() {
        System.out.println("kiwi eat food");
    }

    public void moveUp(Double amount) {
        setyPosition(getyPosition() + amount);
        System.out.println("kiwi moved up");
    }

    public void moveDown(Double amount) {
        setyPosition(getyPosition() - amount);
        System.out.println("kiwi moved down");
    }

    public void moveRight(Double amount) {
        setxPosition(getxPosition() + amount);
        System.out.println("kiwi moved right");
    }

    public void moveLeft(Double amount) {
        setxPosition(getxPosition() - amount);
        System.out.println("kiwi moved left");
    }

    public void makeSound() {
        System.out.println("kiwi makes sound");
    }
}
