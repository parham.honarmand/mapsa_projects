package daneshvar.animal_farm;

public class Eagle extends Bird implements Voiced, Flyable {


    public Eagle(Integer age) {
        super(age);
    }

    void eatFood() {
        System.out.println("eagle eat food");
    }

    public void flyHigher(Double amount) {
        setyPosition(getyPosition() + amount);
        System.out.println("eagle flayed higher");
    }

    public void flyLower(Double amount) {
        setyPosition(getyPosition() - amount);
        System.out.println("eagle flayed lower");
    }

    public void flyRight(Double amount) {
        setxPosition(getxPosition() + amount);
        System.out.println("eagle flayed right");
    }

    public void flyLeft(Double amount) {
        setxPosition(getxPosition() - amount);
        System.out.println("eagle flayed left");
    }

    public void flyForward(Double amount) {
        setzPosition(getzPosition() + amount);
        System.out.println("eagle flayed forward");
    }

    public void moveUp(Double amount) {
        flyHigher(amount);
    }

    public void moveDown(Double amount) {
        flyLower(amount);
    }

    public void moveRight(Double amount) {
        flyRight(amount);
    }

    public void moveLeft(Double amount) {
        flyLeft(amount);
    }

    public void makeSound() {
        System.out.println("eagle makes sound");
    }
}
