package daneshvar.animal_farm;

public abstract class Bird extends Animal {
    protected Double beakLength;

    public Bird(Integer age) {
        super(age);
    }

    public Double getBeakLength() {
        return beakLength;
    }

    public void setBeakLength(Double beakLength) {
        this.beakLength = beakLength;
    }
}
