package daneshvar.animal_farm;

public abstract class Animal implements Movable {
    protected Integer age;
    protected Double xPosition;
    protected Double yPosition;
    protected Double zPosition;

    public Animal(Integer age) {
        this.age = age;
    }

    abstract void eatFood();

    void sleep() {
        System.out.println("Sleeping...zzz");
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getyPosition() {
        return yPosition;
    }

    public void setyPosition(Double yPosition) {
        this.yPosition = yPosition;
    }

    public Double getxPosition() {
        return xPosition;
    }

    public void setxPosition(Double xPosition) {
        this.xPosition = xPosition;
    }

    public Double getzPosition() {
        return zPosition;
    }

    public void setzPosition(Double zPosition) {
        this.zPosition = zPosition;
    }
}
