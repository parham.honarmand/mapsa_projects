package daneshvar.animal_farm;

public class Cat extends Animal implements Voiced {

    public Cat(Integer age) {
        super(age);
    }

    void eatFood() {
        System.out.println("cat eat food");
    }

    public void moveUp(Double amount) {
        setyPosition(getyPosition() + amount);
        System.out.println("cat moved up");
    }

    public void moveDown(Double amount) {
        setyPosition(getyPosition() - amount);
        System.out.println("cat moved down");
    }

    public void moveRight(Double amount) {
        setxPosition(getxPosition() + amount);
        System.out.println("cat moved right");
    }

    public void moveLeft(Double amount) {
        setxPosition(getxPosition() - amount);
        System.out.println("cat moved left");
    }

    public void makeSound() {
        System.out.println("cat makes sound");
    }
}
