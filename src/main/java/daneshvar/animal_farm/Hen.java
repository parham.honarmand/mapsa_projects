package daneshvar.animal_farm;

public class Hen extends Bird implements Voiced {


    public Hen(Integer age) {
        super(age);
    }

    void eatFood() {
        System.out.println("hen eat food");
    }

    public void moveUp(Double amount) {
        setyPosition(getyPosition() + amount);
        System.out.println("hen moved up");
    }

    public void moveDown(Double amount) {
        setyPosition(getyPosition() - amount);
        System.out.println("hen moved down");
    }

    public void moveRight(Double amount) {
        setxPosition(getxPosition() + amount);
        System.out.println("hen moved right");
    }

    public void moveLeft(Double amount) {
        setxPosition(getxPosition() - amount);
        System.out.println("hen moved left");
    }

    public void makeSound() {
        System.out.println("hen makes sound");
    }
}
