package shemshadi.repeated_numbers;

import shemshadi.Utility.Utilities;

import java.util.Hashtable;
import java.util.Map;
import java.util.stream.IntStream;

public class Main {

    static Map<String, Integer> digitList = new Hashtable<>();

    public static void main(String[] args) {

        System.out.println("enter a number : ");
        String number = Utilities.getInstance().getStringFromUser();

        digitCount(number);
        showRepeatedNumber();
    }

    public static void digitCount(String input){
        IntStream.range(0, input.length()).forEach(x -> {
            digitList.put(String.valueOf(input.charAt(x)),
                        ((digitList.containsKey(String.valueOf(input.charAt(x))))?digitList.get(String.valueOf(input.charAt(x))):0) + 1);
        });
    }

    public static void showRepeatedNumber(){
        for (String s : digitList.keySet())
            if(digitList.get(s) != 1)
                System.out.println(s + " " + digitList.get(s));
    }
}
