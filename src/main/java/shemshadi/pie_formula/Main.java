package shemshadi.pie_formula;

public class Main {

    public static void main(String[] args) {
        double pie = 0d;
        int n = 100000000;

        for (int i = 1; i <= n; i++) {
            if (i % 2 != 0)
                pie += 1.0 / (2 * i - 1.0);
            else
                pie -= 1.0 / (2 * i - 1.0);
        }

        pie *= 4d;

        System.out.println(pie);
    }
}
