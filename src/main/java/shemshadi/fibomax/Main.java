package shemshadi.fibomax;

import shemshadi.Utility.Utilities;

public class Main {

    public static void main(String[] args) {

        int input = Integer.parseInt(Utilities.getInstance().getStringFromUser());

        int result = getPreviousFibonachi(input);

        System.out.println(result);
    }

    public static int getPreviousFibonachi(int input){
        int a = 1;
        int b = 1;
        int temp;

        while (b < input){
            temp = b;
            b = a + b;
            a = temp;
        }
        return a;
    }
}
