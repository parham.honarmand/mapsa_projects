package shemshadi.product_digit_speller;

import shemshadi.Utility.Utilities;

import java.util.*;

public class Main {
    private static Map<Integer, String> numbers = new HashMap<>();

    public static void main(String[] args) {
        fillNumbers();
        Stack<Integer> stack = new Stack<>();

        System.out.println("enter number 1 : ");
        int input1 = Utilities.getInstance().getIntFromUser();

        System.out.println("enter number 2 : ");
        int input2 = Utilities.getInstance().getIntFromUser();

        Integer multiple = input1 * input2;

        int counter = multiple.toString().length();

        for (int i = 0; i < counter; i++){
            stack.push(multiple % 10);
            multiple /= 10;
        }

        String result = "";
        while (!stack.isEmpty())
            result += numbers.get(stack.pop()) + " ";

        System.out.println(result);
    }

    private static void fillNumbers(){
        numbers.put(1, "one");
        numbers.put(2, "two");
        numbers.put(3, "three");
        numbers.put(4, "four");
        numbers.put(5, "five");
        numbers.put(6, "six");
        numbers.put(7, "seven");
        numbers.put(8, "eight");
        numbers.put(9, "nine");
        numbers.put(0, "zero");
    }
}
