package shemshadi.sum_of_sum;

import shemshadi.Utility.Utilities;

public class Main {

    public static void main(String[] args) {

        System.out.println("enter a number :");
        String s = Utilities.getInstance().getStringFromUser();



        while (s.length() > 1) {

            if(s.length() % 2 != 0)
                s += "0";

            String[] arr = s.split("");
            s = "";
            for (int i = 0; i < arr.length - 1; i += 2) {
                int a = Integer.parseInt(arr[i]);
                int b = Integer.parseInt(arr[i + 1]);

                s += String.valueOf(a + b);
            }
        }
        System.out.println(s);

    }
}
