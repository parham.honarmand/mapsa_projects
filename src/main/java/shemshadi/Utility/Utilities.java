package shemshadi.Utility;

import java.util.Random;
import java.util.Scanner;

public class Utilities {
    public static final Utilities SCANNER = null;
    private Scanner scanner = new Scanner(System.in);

    public static Utilities getInstance(){
        return (SCANNER == null)? (new Utilities()) :SCANNER;
    }

    public String getStringFromUser(){
        return scanner.nextLine();
    }

    public double getDoubleFromUser(){
        return Double.valueOf(scanner.nextLine());
    }

    public int getIntFromUser(){
        return Integer.parseInt(scanner.nextLine());
    }

    public int getRandom(int n){
        Random ran = new Random();
        int random = ran.nextInt(n);
        return random;
    }
}
