package shemshadi.algoritm;

import shemshadi.Utility.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class sample3 {

    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        int pointer1 = 0;
        int pointer2 = 0;

        System.out.println("enter length of your first array");
        int n = Utilities.getInstance().getIntFromUser();

        System.out.println("enter list 1 :");
        IntStream.range(0, n).forEach(x -> list1.add(Utilities.getInstance().hashCode()));

        System.out.println("enter list 2 :");
        IntStream.range(0, n).forEach(x -> list2.add(Utilities.getInstance().hashCode()));

        for (int i = 0; i < (list1.size() + list2.size()); i++){

            if(pointer1 >= list1.size())
                if(pointer2 >= list2.size())
                    break;
                else {
                    result.add(list2.get(pointer2));
                    pointer2++;
                }

            if(pointer2 >= list2.size())
                if(pointer1 >= list1.size())
                    break;
                else {
                    result.add(list1.get(pointer1));
                    pointer1++;
                }

            if(list1.get(pointer1) <= list2.get(pointer2)){
                result.add(list1.get(pointer1));
                pointer1++;
            }
            else {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }

        System.out.println(result);
    }
}
