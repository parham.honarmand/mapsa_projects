package shemshadi.caesar_cipher;

import shemshadi.Utility.Utilities;

public class Main {

    public static void main(String[] args) {

        System.out.println("enter your text :");
        String input = Utilities.getInstance().getStringFromUser();

        System.out.println("enter your cipher number :");
        int n = Integer.parseInt(Utilities.getInstance().getStringFromUser());

        CaesarCipher cipher = new CaesarCipher();
        System.out.println(cipher.doCipher(input, n));
    }
}
