package shemshadi.caesar_cipher;

import java.util.HashMap;
import java.util.Map;

public class CaesarCipher {
    Map<String, String> map = new HashMap<>();

    public String doCipher(String input, int n){
        for (int i = 0; i < Alphabet.values().length; i++) {
            map.put(Alphabet.values()[i].toString(), (Alphabet.values().length < i ?
                    Alphabet.values()[i + n].toString() : Alphabet.values()[((i + n) % 26)].toString()));
        }

        StringBuilder result = new StringBuilder();
        String[] arr = input.split("");

        for (String s : arr) {
            result.append(map.get(s));
        }
        return result.toString();
    }
}
