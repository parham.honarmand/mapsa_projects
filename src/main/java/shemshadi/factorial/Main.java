package shemshadi.factorial;

import shemshadi.Utility.Utilities;

public class Main {

    public static void main(String[] args) {
        int input = Utilities.getInstance().getIntFromUser();

        System.out.println(factorial(input));
    }

    public static Long factorial(int n){
        if(n == 0)
            return 1l;
        else {
            return n * factorial(n-1);
        }
    }
}
