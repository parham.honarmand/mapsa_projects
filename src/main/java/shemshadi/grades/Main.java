package shemshadi.grades;

import shemshadi.Utility.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    static List<Student> students = new ArrayList<>();

    public static void main(String[] args) {
//        Student student1 = new Student("ali", new Lesson(20d, 17.75), new Lesson(17d, 18.5), new Lesson(18d, 19d));
//        Student student2 = new Student("mohamadreza", new Lesson(19d, 20d), new Lesson(15.75, 12d), new Lesson(15.5, 15d));
//        Student student3 = new Student("javad", new Lesson(12d, 19.5), new Lesson(18.5, 19d), new Lesson(13.75, 18d));
//        Student student4 = new Student("ahmad", new Lesson(20d, 20d), new Lesson(18d, 19d), new Lesson(16d, 17d));
//
//        students = Arrays.asList(student1, student2, student3, student4);

        for (int i = 0; i < 10; i++) {
            System.out.println("enter student name :");
            String name = Utilities.getInstance().getStringFromUser();

            System.out.println("enter math mid term grade :");
            double math_mid_term = Utilities.getInstance().getDoubleFromUser();

            System.out.println("enter math end of term grade :");
            double math_end_of_term = Utilities.getInstance().getDoubleFromUser();

            System.out.println("enter physic mid term grade :");
            double physic_mid_term = Utilities.getInstance().getDoubleFromUser();

            System.out.println("enter physic end of term grade :");
            double physic_end_of_term = Utilities.getInstance().getDoubleFromUser();

            System.out.println("enter chemistry mid term grade :");
            double chemistry_mid_term = Utilities.getInstance().getDoubleFromUser();

            System.out.println("enter chemistry end of term grade :");
            double chemistry_end_of_term = Utilities.getInstance().getDoubleFromUser();

            Student student = new Student(name, new Lesson(math_mid_term, math_end_of_term),
                    new Lesson(physic_mid_term, physic_end_of_term), new Lesson(chemistry_mid_term, chemistry_end_of_term));

            students.add(student);
        }
        menu();
    }

    private static void menu() {
        System.out.println("1 - best student (total average)");
        System.out.println("2 - worst student (total average)");
        System.out.println("3 - average grade in math (total average)");
        System.out.println("4 - average grade in physic (total average)");
        System.out.println("5 - average grade in chemistry (total average)");
        System.out.println("6 - most increased grade per lesson (named and grade increase)");
        System.out.println("7 - math class list sorted in descending grades");
        System.out.println("8 - physic class list sorted in descending grades");
        System.out.println("9 - chemistry class list sorted in descending grades");
        System.out.println("10 - exit");

        System.out.println("Enter a number : ");
        int input = Utilities.getInstance().getIntFromUser();
        List<Student> studentList = null;

        switch (input) {
            case 1:
                Student best = bestAverage();
                System.out.println("best average student is : " + best.getName() + " with average : " + best.getAverage());
                menu();
                break;
            case 2:
                Student worst = worstAverage();
                System.out.println("worst average student is : " + worst.getName() + " with average : " + worst.getAverage());
                menu();
                break;
            case 3:
                System.out.println("math average is : " + Average(Lessons.MATH));
                menu();
                break;
            case 4:
                System.out.println("physic average is : " + Average(Lessons.PHYSIC));
                menu();
                break;
            case 5:
                System.out.println("chemistry average is : " + Average(Lessons.CHEMISTRY));
                menu();
                break;
            case 6:
                studentList = mostIncreasedGrade();
                System.out.println("most math increased is : " + studentList.get(0).getName() + " increased amount is : "
                        + studentList.get(0).differenceBetweenMathGrade());

                System.out.println("most physic increased is : " + studentList.get(1).getName() + " increased amount is : "
                        + studentList.get(1).differenceBetweenPhysicGrade());

                System.out.println("most chemistry increased is : " + studentList.get(2).getName() + " increased amount is : "
                        + studentList.get(2).differenceBetweenChemistryGrade());
                menu();
                break;
            case 7:
                studentList = getStudentsDescending(Lessons.MATH);
                studentList.forEach(x -> System.out.println(x.getName()));
                menu();
                break;
            case 8:
                studentList = getStudentsDescending(Lessons.PHYSIC);
                studentList.forEach(x -> System.out.println(x.getName()));
                menu();
                break;
            case 9:
                studentList = getStudentsDescending(Lessons.CHEMISTRY);
                studentList.forEach(x -> System.out.println(x.getName()));
                menu();
            case 10:
                break;
            default:
                System.out.println("invalid input");
                menu();
        }
    }

    private static Student bestAverage() {
        Student student = students.get(0);
        for (Student x : students)
            if (x.getAverage() > student.getAverage())
                student = x;
        return student;
    }

    private static Student worstAverage() {
        Student student = students.get(0);
        for (Student x : students)
            if (x.getAverage() < student.getAverage())
                student = x;
        return student;
    }

    private static double Average(Lessons type) {
        double average = 0d;
        for (Student student : students)
            switch (type) {
                case MATH:
                    average += student.getMathAverage();
                    continue;
                case PHYSIC:
                    average += student.getPhysicAverage();
                    continue;
                case CHEMISTRY:
                    average += student.getChemistryAverage();
                    continue;
            }
        return average / students.size();
    }

    private static List<Student> mostIncreasedGrade() {
        List<Student> lessonGrades = new ArrayList<>();
        Student mathIncreased = students.get(0);
        Student physicIncreased = students.get(0);
        Student chemistryIncreased = students.get(0);

        for (Student student : students) {
            if (student.differenceBetweenMathGrade() > (mathIncreased.getMath().getFinalTermGrade() -
                    mathIncreased.getMath().getMidTermGrade()))
                mathIncreased = student;

            if (student.differenceBetweenPhysicGrade() > physicIncreased.differenceBetweenPhysicGrade())
                physicIncreased = student;

            if (student.differenceBetweenChemistryGrade() > chemistryIncreased.differenceBetweenChemistryGrade())
                chemistryIncreased = student;
        }
        lessonGrades.add(mathIncreased);
        lessonGrades.add(physicIncreased);
        lessonGrades.add(chemistryIncreased);
        return lessonGrades;
    }

    private static List<Student> getStudentsDescending(Lessons lesson) {
        switch (lesson) {
            case MATH:
                return students.stream().sorted((x, y) -> (x.getMath().getMidTermGrade() < y.getMath().getMidTermGrade()) ? 1 : -1).collect(Collectors.toList());
            case PHYSIC:
                return students.stream().sorted((x, y) -> (x.getPhysic().getMidTermGrade() < y.getPhysic().getMidTermGrade()) ? 1 : -1).collect(Collectors.toList());
            case CHEMISTRY:
                return students.stream().sorted((x, y) -> (x.getChemistry().getMidTermGrade() < y.getChemistry().getMidTermGrade()) ? 1 : -1).collect(Collectors.toList());
            default:
                return null;
        }
    }
}
