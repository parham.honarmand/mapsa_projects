package shemshadi.grades;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Lesson {
    private double midTermGrade;
    private double finalTermGrade;
}
