package shemshadi.grades;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Student {
    private String name;
    private Lesson math;
    private Lesson physic;
    private Lesson chemistry;

    public double getAverage(){
        double sum = math.getFinalTermGrade() + math.getMidTermGrade() + physic.getMidTermGrade() +
                physic.getFinalTermGrade() + chemistry.getFinalTermGrade() + chemistry.getMidTermGrade();

        return sum / 6d;
    }

    public double getMathAverage(){
        double sum = math.getFinalTermGrade() + math.getMidTermGrade();
        return sum / 2d;
    }

    public double getPhysicAverage(){
        double sum = physic.getMidTermGrade() + physic.getFinalTermGrade();
        return sum / 2d;
    }

    public double getChemistryAverage(){
        double sum = chemistry.getMidTermGrade() + chemistry.getFinalTermGrade();
        return sum / 2d;
    }

    public double differenceBetweenMathGrade(){
        return math.getFinalTermGrade() - math.getMidTermGrade();
    }

    public double differenceBetweenPhysicGrade(){
        return physic.getFinalTermGrade() - physic.getMidTermGrade();
    }

    public double differenceBetweenChemistryGrade(){
        return chemistry.getFinalTermGrade() - chemistry.getMidTermGrade();
    }
}
