package shemshadi.keypad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Keypad {

    private Map<String, String> cells;
    private Map<String, String> reverseCells;

    Keypad(){
        cells = new HashMap<>();
        reverseCells = new HashMap<>();

        cells.put(".", "1");
        cells.put("!", "11");
        cells.put("?", "111");
        cells.put("a", "2");
        cells.put("b", "22");
        cells.put("c", "222");
        cells.put("d", "3");
        cells.put("e", "33");
        cells.put("f", "333");
        cells.put("g", "4");
        cells.put("h", "44");
        cells.put("i", "444");
        cells.put("j", "5");
        cells.put("k", "55");
        cells.put("l", "555");
        cells.put("m", "6");
        cells.put("n", "66");
        cells.put("o", "666");
        cells.put("p", "7");
        cells.put("q", "77");
        cells.put("r", "777");
        cells.put("s", "8");
        cells.put("t", "88");
        cells.put("u", "888");
        cells.put("v", "9");
        cells.put("w", "99");
        cells.put("x", "999");
        cells.put("y", "0");
        cells.put("z", "00");
        cells.put(" ", "000");

        for (String s : cells.keySet()) {
            reverseCells.put(cells.get(s), s);
        }
    }

    public String getCodeFromString(String input) {
        char[] chars = input.toCharArray();
        StringBuilder s = new StringBuilder();
        String append = "";
        char previous = '-';

        for (char c : chars) {
            append = cells.get(String.valueOf(c));
            if(append.charAt(0) == previous)
                s.append(" ");
            s.append(append);
            previous = append.charAt(0);
        }

        if(!s.toString().isEmpty())
            return s.toString();
        else {
            return "invalid input";
        }
    }

    public String getStringFromCode(String code){
        List<String> codes = splitStringByCharacter(code);
        StringBuilder result = new StringBuilder();

        for (String c : codes) {
            result.append(reverseCells.get(String.valueOf(c)));
        }
        return result.toString();
    }

    public List<String> splitStringByCharacter(String string){

        List<String> resultStrings = new ArrayList<>();
        StringBuilder currentString = new StringBuilder();

        for (int pointer = 0; pointer < string.length(); pointer++){

            currentString.append(string.charAt(pointer));

            if (pointer == string.length() - 1
                    || currentString.charAt(0) != string.charAt(pointer + 1)) {
                if(!" ".equals(currentString.toString()))
                    resultStrings.add(currentString.toString());
                currentString = new StringBuilder();
            }
        }

        return resultStrings;
    }
}
