package shemshadi.keypad;

import shemshadi.Utility.Utilities;

public class Main {

    private static final Keypad KEYPAD = new Keypad();

    public static void main(String[] args) {
        menu();
    }

    private static void menu(){

        System.out.println("Welcome");
        System.out.println("1 = convert text to code");
        System.out.println("2 = convert code to text");
        System.out.println("3 = exit");

        int input = 0;

        try {
            input = Integer.parseInt(Utilities.getInstance().getStringFromUser());
        }catch (NumberFormatException e){
            System.out.println("invalid input");
        }

        switch (input){
            case 1:
                convertStringToCode();
                menu();
            case 2:
                convertCodeToString();
                menu();
            case 3:
                break;
            default:
                System.out.println("invalid input");
                menu();
        }
    }

    private static void convertStringToCode(){
        System.out.println("enter your text");
        String input = Utilities.getInstance().getStringFromUser();
        String code = KEYPAD.getCodeFromString(input);
        System.out.println(code);
    }

    private static void convertCodeToString(){
        System.out.println("enter your code");
        String input = Utilities.getInstance().getStringFromUser();
        String result = KEYPAD.getStringFromCode(input);
        System.out.println(result);
    }
}
