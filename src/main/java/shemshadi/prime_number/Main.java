package shemshadi.prime_number;

import shemshadi.Utility.Utilities;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("enter a number :");
        int input = Utilities.getInstance().getIntFromUser();

        getLowerPrimeNumber(input).forEach(x -> System.out.println(x));
    }

    private static List<Integer> getLowerPrimeNumber(int input) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < input; i++)
                if(isPrimeNumber(i))
                    list.add(i);
        return list;
    }

    private static boolean isPrimeNumber(int input){
        if(input == 2 || input == 1)
            return true;
        for (int i = 2; i <= input / 2; i++)
            if(input % i == 0)
                return false;
        return true;
    }
}
