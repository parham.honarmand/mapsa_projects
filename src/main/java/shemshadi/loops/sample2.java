package shemshadi.loops;

import shemshadi.Utility.Utilities;

public class sample2 {

    public static void main(String[] args) {
        String input;

        //with do while
//        do {
//            System.out.println(input);
//        } while (!"#".equals(input = scanner.nextLine()));


//        with while
        while (!"#".equals(input = Utilities.getInstance().getStringFromUser())){
            System.out.println(input);
        }

    }
}
