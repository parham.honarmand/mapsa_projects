package shemshadi.loops;

import shemshadi.Utility.Utilities;

import java.util.stream.IntStream;

public class sample1 {

    public static void main(String[] args) {

        int n = Utilities.getInstance().getIntFromUser();
        for (int i = 1; i <= n; i++){
            printA(i);
            printB(n - i + 1);
        }
    }

    public static void printA(int n){
        IntStream.range(0, n).forEach(x -> System.out.println("A"));
    }

    public static void printB(int n){
        IntStream.range(0, n).forEach(x -> System.out.println("B"));
    }
}
