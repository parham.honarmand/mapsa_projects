package shemshadi.sum;

import shemshadi.Utility.Utilities;

import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        System.out.println("enter number 1 :");
        String number1 = Utilities.getInstance().getStringFromUser();

        System.out.println("enter number 2 :");
        String number2 = Utilities.getInstance().getStringFromUser();

        System.out.println(sum(number1, number2));
    }

    public static String sum(String number1, String number2) {

        int[] num1 = new int[number1.length()];
        int[] num2 = new int[number2.length()];

        IntStream.range(0, number1.length()).forEach(x -> num1[x] = Integer.valueOf(number1.split("")[x]));
        IntStream.range(0, number2.length()).forEach(x -> num2[x] = Integer.valueOf(number2.split("")[x]));

        int[] sum = new int[(num1.length > num2.length ? ((num1[0] + num2[0]) > 9)? num1.length +1 : num1.length :
                ((num1[0] + num2[0]) > 9)? num2.length +1 : num2.length)];

        for (int num1Pointer = num1.length - 1, num2Pointer = num2.length - 1, sumPointer = sum.length - 1;
             num1Pointer >= 0 || num2Pointer >= 0; num1Pointer--, num2Pointer--, sumPointer--) {
            if(num1Pointer < 0)
                sum[sumPointer] += num2[num2Pointer];
            else if(num2Pointer < 0)
                sum[sumPointer] += num1[num1Pointer];
            else
                sum[sumPointer] += num1[num1Pointer] + num2[num2Pointer];

            if (sum[sumPointer] >= 10) {
                sum[sumPointer] %= 10;
                sum[sumPointer - 1]++;
            }
        }
        StringBuilder str = new StringBuilder();
        IntStream.range(0, sum.length).forEach(x -> str.append(sum[x]));

        return str.toString();
    }
}
