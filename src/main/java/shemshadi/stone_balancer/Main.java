package shemshadi.stone_balancer;

import shemshadi.Utility.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("enter stones number :");
        int count = Utilities.getInstance().getIntFromUser();

        String[] input = new String[count];

        for (int i = 0; i < count; i++) {
            System.out.println("enter stone name");
            input[i] = Utilities.getInstance().getStringFromUser();
        }

        List<Integer> inputWeight = new ArrayList<>();
        List<Integer> iterated = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            System.out.println("enter stone weight :");
            inputWeight.add(Utilities.getInstance().getIntFromUser());
        }

        Collections.sort(inputWeight);
        System.out.println(inputWeight);

        inputWeight.forEach(x -> iterated.add(x));

        int sum = inputWeight.stream().reduce((x, y) -> x + y).get();
        System.out.println(sum);

        List<Pair<List<Integer>, Integer>> pairList = new ArrayList<>();

        for (int k = 0; k < inputWeight.size(); k++) {

            List<Integer> subList = new ArrayList<>();

            for (int l = 0; l < iterated.size() - 1; l++) {
                subList.add(iterated.get(l));
                for (int m = subList.size(); m < iterated.size(); m++) {
                    Pair<List<Integer>, Integer> pair = new Pair<>(new ArrayList<>(subList), iterated.get(m));
                    pairList.add(pair);
                }
            }
            iterated.remove(0);
        }

        Pair<List<Integer>, Integer> result = null;

        int temp = sum / 2;
        Label : for (int i = 0; i < sum / 2; i++) {

            for (Pair<List<Integer>, Integer> integers : pairList) {

                int subSum = integers.getFirst().stream().reduce((x, y) -> x + y).get() + integers.getSecond();

                if (subSum == temp) {
                    result = integers;
                    break Label;
                }
            }
            temp--;
        }

        result.getFirst().forEach(x -> list1.add(x));
        list1.add(result.getSecond());

        System.out.println(list1);
        System.out.println(subtract(inputWeight, list1));
    }

    private static List<Integer> subtract(List<Integer> list1, List<Integer> list2){
        List<Integer> result = list1;

        HERE : for (Integer integer : list2) {
            for (Integer integer1 : list1)
                if(integer == integer1){
                    result.remove(integer);
                    continue HERE;
                }
        }
        return result;
    }
}
