package shemshadi.number_in_word;
import shemshadi.Utility.Utilities;
import java.util.HashMap;
import java.util.Map;

public class Main {
    static Map<Integer, String> list = new HashMap<>();

    public static void main(String[] args) {
        fillWordList();
        System.out.println("enter a number :");
        int input = Utilities.getInstance().getIntFromUser();
        String result = getWordFromNumber(input);
        System.out.println(result);
    }

    private static String getWordFromNumber(int input) {
        String result = "";
        if(input/100 >= 1) {
            result += list.get((input / 100) * 100);
            if(input % 100 != 0)
                result += "_";
            else
                return result;
        }
        if(input % 100 < 20) {
            if(input % 100 > 9)
                result += list.get(input % 100);
            else
                result += list.get(input % 10);
            return result;
        }
        if(input/10 > 1) {
            result += list.get(((input % 100) / 10) * 10);
            if (input % 10 != 0)
                result += "_";
        }
        if(input % 10 != 0)
            result += list.get(input % 10);
        return result;
    }

    private static void fillWordList(){
        list.put(1, "one");
        list.put(2, "two");
        list.put(3, "three");
        list.put(4, "four");
        list.put(5, "five");
        list.put(6, "six");
        list.put(7, "seven");
        list.put(8, "eight");
        list.put(9, "nine");
        list.put(10, "ten");
        list.put(11, "eleven");
        list.put(12, "twelve");
        list.put(13, "thirteen");
        list.put(14, "fourteen");
        list.put(15, "fifteen");
        list.put(16, "sixteen");
        list.put(17, "seventeen");
        list.put(18, "eighteen");
        list.put(19, "nineteen");
        list.put(20, "twenty");
        list.put(30, "thirty");
        list.put(40, "forty");
        list.put(50, "fifty");
        list.put(60, "sixty");
        list.put(70, "seventy");
        list.put(80, "eighty");
        list.put(90, "ninety");
        list.put(100, "one_hundred");
        list.put(200, "two_hundred");
        list.put(300, "three_hundred");
        list.put(400, "four_hundred");
        list.put(500, "five_hundred");
        list.put(600, "six_hundred");
        list.put(700, "seven_hundred");
        list.put(800, "eight_hundred");
        list.put(900, "nine_hundred");
    }
}
