package shemshadi.Bin2Dec;

import shemshadi.Utility.Utilities;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        System.out.println("enter a binary number");
        String input = Utilities.getInstance().getStringFromUser();

        Stack<Integer> number = new Stack<>();
        int base = 1;
        for (int i = 0; i < input.length(); i++) {
            number.push((Integer.valueOf(String.valueOf(input.charAt(input.length() - i - 1))) * base));
            base *= 2;
        }

        int result = 0;

        while (!number.isEmpty())
            result = result + number.pop();

        System.out.println(result);
    }
}
