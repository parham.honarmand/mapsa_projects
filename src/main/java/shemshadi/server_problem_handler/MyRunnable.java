package shemshadi.server_problem_handler;

public class MyRunnable implements Runnable {
    int counter;

    MyRunnable(int counter){
        this.counter = counter;
    }

    @Override
    public void run() {
        Server.getInstance().addToQueue(new Client(counter));
    }
}
