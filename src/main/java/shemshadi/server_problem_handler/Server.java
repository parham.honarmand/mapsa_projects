package shemshadi.server_problem_handler;

import java.util.concurrent.*;

public class Server implements Runnable{
    private static Server server;
    private BlockingQueue<Client> clients = new LinkedBlockingQueue<>(20);

    private Server() {
    }

    public static Server getInstance() {
        if (server == null)
            server = new Server();
        return server;
    }

    public void addToQueue(Client client) {
        try {
            clients.put(client);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void serve(String str) {
        try {
            Thread.sleep(10);
            System.out.println(str);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true)
            try {
                serve(clients.take().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}

