package shemshadi.server_problem_handler;

import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {

        ExecutorService threadPool = Executors.newFixedThreadPool(100);
        Thread t = new Thread(Server.getInstance());
        t.start();

        for (int i = 0; i < 1000; i++)
            threadPool.execute(new MyRunnable(i));

        threadPool.shutdown();
    }
    }
