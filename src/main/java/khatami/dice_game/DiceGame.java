package khatami.dice_game;

import java.util.Random;

public class DiceGame {

    private State turn;

    DiceGame(State turn){
        this.turn = turn;
    }

    public void changeTurn(){
        if(turn == State.PLAYER)
            turn = State.COMPUTER;
        else
            turn = State.PLAYER;
    }

    public int random(){
        Random random = new Random();
        int ran = random.nextInt(6);
        return ran+1;
    }

    public State checkForWinner(int player, int computer){
        if(player>computer)
            return State.PLAYER;
        else if(player<computer)
            return State.COMPUTER;

        return State.EVEN;
    }


    public State getTurn() {
        return turn;
    }
}
