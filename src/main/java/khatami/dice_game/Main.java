package khatami.dice_game;

import shemshadi.Utility.Utilities;

public class Main {

    static DiceGame diceGame = new DiceGame(State.PLAYER);

    public static void main(String[] args) {

        System.out.println("enter number of play :");
        int numberOfPlay = Utilities.getInstance().getIntFromUser();

        int playerWins = 0;
        int computersWins = 0;
        int even = 0;

        int player = 0;
        int computer = 0;

        for (int i = 0; i < numberOfPlay; i++){
            for (int j = 0; j < 2; j++) {
                switch (diceGame.getTurn()) {
                    case PLAYER:
                        player = playersTurn();
                        System.out.println("Player : " + player);
                        diceGame.changeTurn();
                        break;
                    case COMPUTER:
                        computer = diceGame.random();
                        System.out.println("Computer : " + computer);
                        diceGame.changeTurn();
                        break;
                }
            }
            switch (diceGame.checkForWinner(player, computer)){
                case COMPUTER:
                    computersWins++;
                    break;
                case PLAYER:
                    playerWins++;
                    break;
                default:
                    even++;
                    System.out.println("dice is even");
            }

            System.out.println("player score : " + playerWins);
            System.out.println("computer score : " + computersWins);
            System.out.println("evens : " + even);
        }

        checkForGameWinner(computersWins, playerWins);
    }

    public static int playersTurn(){
        System.out.println("its your turn enter anything to play :");
        String s = Utilities.getInstance().getStringFromUser();
        return diceGame.random();
    }

    public static void checkForGameWinner(int computersWins, int playerWins){
        if(computersWins> playerWins)
            System.out.println("computer wins");
        else if (playerWins>computersWins)
            System.out.println("player wins");
        else
            System.out.println("game is even");
    }
}
