package khatami.dice_game;

import shemshadi.Utility.Utilities;

import java.util.Random;

public class ThreadMain {

    static int playerWins = 0;
    static int computerWins = 0;
    static int even = 0;
    public static void main(String[] args) {
        int number = Utilities.getInstance().getIntFromUser();

        Runnable task = () -> {
                int player = 0;
                int computer = 0;

                System.out.println("player turn enter to play :");
                player = getRandom();
                computer = getRandom();
                System.out.println("player : " + player);
                System.out.println("computer : " + computer);
                if(player>computer)
                    playerWins++;
                else if (computer>player)
                    computerWins++;
                else
                    even++;
        };

        Thread thread = new Thread(task);

        for (int i = 0; i<number; i++){
            thread.run();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(playerWins>computerWins)
            System.out.println("\nplayer wins : " + playerWins);
        else if (computerWins>playerWins)
            System.out.println("computers win : " + computerWins);
        else
            System.out.println("even");
 
    }

    public static int getRandom(){
        Random random = new Random();
        return random.nextInt(6);
    }
}
