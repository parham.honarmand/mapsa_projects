package khatami.sweing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MyFrame extends JFrame {

    MyFrame(String title){
        super(title);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JButton button = new JButton("salam");
        JButton button1 = new JButton("hello");
        JButton button2 = new JButton("marhaba");
        JTextField text = new JFormattedTextField();

        setBounds(200, 200, 200, 200);
        text.setColumns(14);
        panel.setLayout(new GridLayout(2, 2));
        panel.add(button);
        panel.add(button1);
        panel.add(button2);
        panel.add(text);
        add(panel);
        setVisible(true);
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                text.setText(button.getText());
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        button1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                text.setText(button1.getText());
            }
        });

        button2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                text.setText(button2.getText());
            }
        });
    }
}
