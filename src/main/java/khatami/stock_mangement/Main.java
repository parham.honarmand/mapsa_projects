package khatami.stock_mangement;

import java.util.ArrayList;
import java.util.List;

public class Main {

    static Long amount = 10_000l;
    public static void main(String[] args) {

        Long[] stockPrice = {200l, 300l, 400l, 50000l, 600l};
        String[] stockName = {"metal", "iron", "gold", "gas", "petrol"};
        String[] stockHolderName = {"ali", "jack", "joe", "sarah", "parham"};

        List<StockHolder> stockHolders = new ArrayList<>();

        Thread thread = new Thread(()->{
            for (int i = 0; i < stockHolderName.length; i++) {
                Stock stock = new Stock(i+1 , stockName[i], stockPrice[i],i + 1);
                StockHolder stockHolder = new StockHolder(stockHolderName[i], stockPrice[i] + amount, stock);
                stockHolders.add(stockHolder);
            }
        });
        thread.start();

        try {
            thread.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread thread1 = new Thread(() -> {
            checkForLegal(stockHolders);
        });

        thread1.start();
    }

    public static void checkForLegal(List<StockHolder> stockHolders){
        for (StockHolder stockHolder : stockHolders)
            if(stockHolder.budget > stockHolder.stock.price * stockHolder.stock.count)
                System.out.println(stockHolder);
    }
}
