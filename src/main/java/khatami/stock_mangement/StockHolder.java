package khatami.stock_mangement;

public class StockHolder {
    String name;
    Long budget;
    Stock stock;

    public StockHolder(String name, Long budget, Stock stock) {
        this.name = name;
        this.budget = budget;
        this.stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBudget() {
        return budget;
    }

    public void setBudget(Long budget) {
        this.budget = budget;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "StockHolder{" +
                "name='" + name + '\'' +
                ", budget=" + budget +
                ", stock=" + stock +
                '}';
    }
}
