package khatami.stock_mangement;

public class Stock {
    int id;
    String title;
    Long price;
    int count;

    public Stock(int id, String title, Long price, int count) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
