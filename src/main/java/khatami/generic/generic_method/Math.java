package khatami.generic.generic_method;

public class Math extends Parent {

    int y;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
