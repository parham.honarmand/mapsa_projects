package khatami.generic.generic_method;

public class GenericType<T extends Parent> {

    T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
