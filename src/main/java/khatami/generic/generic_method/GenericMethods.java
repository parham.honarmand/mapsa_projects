package khatami.generic.generic_method;

public class GenericMethods {

    public static <T extends Parent> boolean isEquals(GenericType<T> t1, GenericType<T> t2){
        return t1.equals(t2);
    }

    public static void main(String[] args) {
        GenericType<Math> g1 = new GenericType<>();
        g1.setT(new Math());

        GenericType<Math> g2 = new GenericType<>();
        g2.setT(new Math());

        boolean b = GenericMethods.isEquals(g1, g2);
        System.out.println(b);
    }
}
