package khatami.generic;

public class Practice1<T extends Number> {

    T number;

    public T getNumber() {
        return number;
    }

    public void setNumber(T number) {
        this.number = number;
    }

    public static void main(String[] args) {
        Practice1<Integer> practice1 = new Practice1<>();
        practice1.setNumber(23);
        System.out.println(practice1.getNumber());

        Practice1<Double> practice2 = new Practice1<Double>();
        practice2.setNumber(3.324);
        System.out.println(practice2.getNumber());
    }
}
