package khatami.generic.java8;

import java.util.*;

public class Java8Future {

    public static List<Developer> developers = new ArrayList<>();

    public static void main(String[] args) {
        fillDeveloperList();

        developers.forEach(x -> System.out.println(x));
        System.out.println("after");

        developers.sort((x, y) -> Long.compare(x.salary, y.salary));
        System.out.println("before :");

        developers.forEach(x -> System.out.println(x));

        System.out.println("-----------");

        Developer[] array = developers.toArray(new Developer[developers.size()]);

        Arrays.parallelSort(array, Comparator.comparing(Developer::getName));
        Arrays.asList(array).forEach(System.out::println);

        Collections.sort(developers, (x, y) -> x.getName().compareTo(y.getName()));
    }

    public static void fillDeveloperList(){
        developers.add(new Developer("p", 2000L));
        developers.add(new Developer("a", 1000L));
        developers.add(new Developer("s", 2500L));
        developers.add(new Developer("d", 3000L));
        developers.add(new Developer("f", 4000L));
        developers.add(new Developer("g", 1500L));
        developers.add(new Developer("h", 4500L));
    }
}
