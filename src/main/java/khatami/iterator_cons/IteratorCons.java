package khatami.iterator_cons;

import java.util.ArrayList;
import java.util.List;

public class IteratorCons {
    public static void main(String[] args) {
        List<Integer> grades = new ArrayList<>();
        boolean missValue = true;

        grades.add(11);
        grades.add(17);
        grades.add(12);
        grades.add(18);

        //first
        for (Integer grade : grades) {
            if(missValue){
                grades.add(15);
                missValue = false;
            }
        }

        //second
        for (int i = 0; i < grades.size(); i++) {
            if(missValue){
                grades.add(15);
                missValue = false;
            }
        }

        //third
        List<Integer> gradeList = new ArrayList<>();

        for (Integer grade : grades) {
            if(missValue){
                gradeList.add(15);
                missValue = false;
            }
            gradeList.add(grade);
        }
    }
}
