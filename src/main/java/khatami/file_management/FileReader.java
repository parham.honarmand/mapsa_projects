package khatami.file_management;

import java.io.*;

public class FileReader {

    public static void main(String[] args) {

        File file = new File("test.txt");

        try(BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(file))){
            String string;
             while ((string = bufferedReader.readLine()) != null){
                 System.out.println(string);
             }
        }catch (Exception e){

        }
    }
}
