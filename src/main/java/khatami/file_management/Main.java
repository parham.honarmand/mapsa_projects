package khatami.file_management;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Main {

    public static void main(String[] args) {

        try(FileInputStream fileInputStream = new FileInputStream(new File("input.txt"));
            FileOutputStream fileOutputStream = new FileOutputStream(new File("output.txt"))){

            int c ;
            while ((c = fileInputStream.read()) != -1){
                fileOutputStream.write(c);
            }

        }catch (Exception e){

        }
    }
}
