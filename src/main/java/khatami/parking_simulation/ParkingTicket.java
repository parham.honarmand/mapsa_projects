package khatami.parking_simulation;

public class ParkingTicket {
    private ParkedCar parkedCar;
    private PoliceOfficer policeOfficer;
    private int minute;
    private double cost;

    private final int BASE_ENTERY = 25_000;
    private final int HOURLY = 14_000;

    public ParkingTicket(ParkedCar parkedCar, PoliceOfficer policeOfficer, int minute) {
        this.parkedCar = parkedCar;
        this.policeOfficer = policeOfficer;
        this.minute = minute;

        calculateCost(new ParkingMeter(70));
    }

    public void calculateCost(ParkingMeter parkingMeter){
        int hour = minute / 60;
        int paidHour = parkingMeter.purchesedMinute / 60;

        if(hour > paidHour)
            cost = BASE_ENTERY + ((hour - paidHour) * HOURLY);
    }

    public ParkedCar getParkedCar() {
        return parkedCar;
    }

    public PoliceOfficer getPoliceOfficer() {
        return policeOfficer;
    }

    public int getMinute() {
        return minute;
    }

    public double getCost() {
        return cost;
    }
}
