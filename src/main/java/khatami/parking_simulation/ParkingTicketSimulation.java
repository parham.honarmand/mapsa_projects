package khatami.parking_simulation;

public class ParkingTicketSimulation {

    public static void main(String[] args) {
        ParkedCar car1 = new ParkedCar("toyota", "2020", "sd54a3", Colors.GREEN, "234g3");
        PoliceOfficer policeOfficer = new PoliceOfficer("joe", "yg23u4gu23");

        ParkingTicket ticket = new ParkingTicket(car1, policeOfficer, 130);

        System.out.println(policeOfficer.getCost(ticket));
    }
}
