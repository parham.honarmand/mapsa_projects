package khatami.parking_simulation;

public class ParkedCar {
    private String companyName;
    private String model;
    private String licenceNumber;
    private Colors color;
    private String plaque;

    public ParkedCar(String companyName, String model, String licenceNumber, Colors color, String plaque) {
        this.companyName = companyName;
        this.model = model;
        this.licenceNumber = licenceNumber;
        this.color = color;
        this.plaque = plaque;
    }
}
