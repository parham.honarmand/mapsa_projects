package khatami.parking_simulation;

public class PoliceOfficer {
    public String name;
    public String badgeNumber;

    public PoliceOfficer(String name, String badgeNumber) {
        this.name = name;
        this.badgeNumber = badgeNumber;
    }

    public int getCost(ParkingTicket parkingTicket){
        return (int) parkingTicket.getCost();
    }
}
