package khatami.bank_card_number;

import shemshadi.Utility.Utilities;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static Map<String, String> map = new HashMap<>();

    public static void add(){
        map.put("603799", "melli");
        map.put("589210", "sepah");
        map.put("603769", "saderat");
        map.put("627961", "sanaat");
        map.put("603770", "keshavarzi");
        map.put("628023", "maskan");
        map.put("627760", "post");
        map.put("502908", "toseh");
        map.put("627412", "eqtesad");
        map.put("622106", "parsian");
        map.put("502229", "pasargad");
        map.put("621986", "saman");
        map.put("610433", "mellat");
        map.put("627353", "tejarat");
        map.put("639346", "sina");
        map.put("589463", "refah");
        map.put("639370", "mehr");
        map.put("627381", "ansar");
        map.put("502806", "shahr");
    }

    public static void main(String[] args) {
        add();
        String s = Utilities.getInstance().getStringFromUser();

        String card = "";

        String[] number = s.split("-");

        for (int i = 0; i < number.length; i++)
            card += number[i];

        String num = card.substring(0, 6);
        String res = map.get(num);

        System.out.println(res);
    }
}
